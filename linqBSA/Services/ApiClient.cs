﻿using linqBSA.Data;
using linqBSA.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace linqBSA.Models
{
    public class ApiClient : IApiClient
    {

        public ApiClient()
        {
            _httpClient = new HttpClient(){};
        }
        
        private readonly HttpClient _httpClient;

        /*public async Task<List<Project>> GetDataHirerarchy()
        {
            var users = await ReceiveApiData<User>("Users");
            var teams = await ReceiveApiData<Team>("Teams");
            var tasks = await ReceiveApiData<Task>("Tasks");
            var projects = await ReceiveApiData<Project>("Projects");




            //var hierarchy = (from project in projects
            //                 join task in tasks on project.Id equals task.ProjectId into tasksGroup
            //                 join author in users on project.AuthorId equals author.Id
            //                 join team in teams on project.TeamId equals team.Id

            //                 select new Project()
            //                 {
            //                     Id = project.Id,
            //                     AuthorId = project.AuthorId,
            //                     TeamId = project.TeamId,
            //                     Name = project.Name,
            //                     Description = project.Description,
            //                     Deadline = project.Deadline,
            //                     CreatedAt = project.CreatedAt,
            //                     Author = new User()
            //                     {
            //                         Id = author.Id,
            //                        TeamId = author.TeamId,
            //                         FirstName = author.FirstName,
            //                         LastName = author.LastName,
            //                         Email = author.Email,
            //                         RegisteredAt = author.RegisteredAt,
            //                         BirthDay = author.BirthDay,
            //                         Projects = projects.Where(p => p.TeamId == team.Id).ToList()
            //                     },
            //                        Team = new Team()
            //                        {
            //                            Id = team.Id,
            //                            Name = team.Name,
            //                            CreatedAt = team.CreatedAt,
            //                            Performers = (from performer in users
            //                                        where performer.TeamId == project.TeamId
            //                                        select new User()
            //                                        {
            //                                            Id = performer.Id,
            //                                            Team = teams.FirstOrDefault(t => t.Id == performer.Id),
            //                                            TeamId = performer.TeamId,
            //                                            Projects = projects.Where(p => p.AuthorId == performer.Id).ToList(),
            //                                            FirstName = performer.FirstName,
            //                                            LastName = performer.LastName,
            //                                            Email = performer.Email,
            //                                            RegisteredAt = performer.RegisteredAt,
            //                                            BirthDay = performer.BirthDay,
            //                                            Tasks = tasks.Where(t => t.PerformerId == performer.Id).ToList()
            //                                        }).ToList(),
            //                        },
            //                         Tasks = (from task in tasksGroup
            //                                  select new Task()
            //                                  {
            //                                      Id = task.Id,
            //                                      ProjectId = task.ProjectId,
            //                                      PerformerId = task.PerformerId,
            //                                      Performer = users.FirstOrDefault(t => t.Id == task.PerformerId),
            //                                      Name = task.Name,
            //                                      Description = task.Description,
            //                                      State = task.State,
            //                                      CreatedAt = task.CreatedAt,
            //                                      FinishedAt = task.FinishedAt,
            //                                      Project = projects.FirstOrDefault(p => p.Id == task.ProjectId),
            //                                  }).ToList()
            //                 }).ToList(); ;


            //return hierarchy;
        }*/


        public async Task<List<T>> ReceiveApiData<T>(string endpoint) where T : class
        {
            var response = await _httpClient.GetAsync(Constants.apilink + endpoint);

            var json = await response.Content.ReadAsStringAsync();

            var result = JsonSerializer.Deserialize<List<T>>(json);
            return result;
        }


        

    }
}
