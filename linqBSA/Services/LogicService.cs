﻿using linqBSA.Interfaces;
using linqBSA.Models;
using linqBSA.Models.HirerarchyModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Services
{
    class LogicService : ILogicService
    {
        private IEnumerable<Models.Task> _tasks;
        private IEnumerable<Project> _projects;
        private IEnumerable<Team> _teams;
        private IEnumerable<User> _users;

        public LogicService()
        {
            GetAllLists();
        }

        private async void GetAllLists()
        {
            var client = new ApiClient();

            _tasks = await client.ReceiveApiData<Models.Task>("Tasks") ?? new List<Models.Task>();
            _projects = await client.ReceiveApiData<Project>("Projects") ?? new List<Project>();
            _teams = await client.ReceiveApiData<Team>("Teams") ?? new List<Team>();
            _users = await client.ReceiveApiData<User>("Users") ?? new List<User>();
        }
        //1
        public Dictionary<string, int> GetTasksByUser(int id)
        {
            var userProjects = _projects
                                .Where(project => project.authorId == id);

            return userProjects.GroupJoin(_tasks,
                            project => project.id,
                            task => task.projectId,
                            (project, tasks) => (project.name, tasks.Count()))
                    .ToDictionary(key => key.name, value => value.Item2);
        }

        //2
        public IEnumerable<Models.Task> GetTasksByLenght(int id)
        {


            return _tasks.Where(task => task.performerId == id && task.name.Length < Constants.maxNameLenght);
        }

        //3
        public IEnumerable<Models.Task> GetFinishedTasks(int id)
        {
            return _tasks.Where(task => task.performerId == id && task.finishedAt.Value.Year == DateTime.Now.Year && task.finishedAt != null);
        }

        //4
        public IEnumerable<(int, string, List<User>)> GetTeamsOrderedAndSorted()
        {
            return _teams.GroupJoin(_users,
                            team => team.id,
                            user => user.teamId,
                            (team, users) => (team.id, team.name, users
                                                .Where(user => user.birthDay.Year < Constants.olderTen)
                                                .OrderByDescending(user => user.registeredAt)
                                                .ToList()
                                                ));
        }

        //5
        public IEnumerable<User> GetUsersSortedByAlphabet()
        {
            return _users.OrderBy(user => user.firstName).GroupJoin(_tasks,
                            user => user.id,
                            task => task.performerId,
                            (user, tasks) => {
                                user.Tasks = tasks.OrderByDescending(task => task.name.Length).ToList();
                                return user;
                            });
        }

        //6
        public UsersLastProjectWithTasksandLonger GetLastProjectCountAndLongerTasks(int userId)
        {
            var lastProjectStruct = new UsersLastProjectWithTasksandLonger();

            var res = _users.Where(user => user.id == userId)
                            .GroupJoin(_projects,
                                        user => user.id,
                                        project => project.authorId,

                                        (user, projects) => {
                                            lastProjectStruct.User = user;
                                            lastProjectStruct.LastProject = projects.OrderByDescending(project => project.createdAt)
                                                                                        .FirstOrDefault();
                                            lastProjectStruct.CountTasksPerLastProject = projects.Where(project => project.id == lastProjectStruct.LastProject.id)
                                                                                                    .GroupJoin(_tasks,
                                                                                                                project => project.id,
                                                                                                                task => task.projectId,
                                                                                                                (project, tasks) => { return tasks.Count(); }).FirstOrDefault();
                                            return user;
                                        }).GroupJoin(_tasks,
                                                        user => user.id,
                                                        task => task.performerId,

                                                        (user, tasks) => {
                                                            lastProjectStruct.NotFinishedOrCanceledTasks = tasks
                                         .Where(task => task.State == StateOfTask.Finished || task.State == StateOfTask.InProgress).Count();
                                                            lastProjectStruct.LongestTask = tasks.OrderByDescending(task => task.finishedAt - task.createdAt)
                                                                                                   .FirstOrDefault();
                                                            return lastProjectStruct;
                                                        }).FirstOrDefault();
            return res;
        }

        //7
        public IEnumerable<LongestAndShortestTaskWithUsers> GetProjectLogestAndShortestTaskAndUsersAmount()
        {
            var currentProjectLongest = new LongestAndShortestTaskWithUsers();

            var res = _projects.GroupJoin(_tasks,
                                            project => project.id,
                                            task => task.projectId,
                                            (project, tasks) => {
                                                currentProjectLongest.Project = project;
                                                currentProjectLongest.LongestTaskByDescription = tasks.OrderByDescending(task => task.description.Length)
                                                                                                        .FirstOrDefault();
                                                currentProjectLongest.ShortestTaskByName = tasks.OrderBy(task => task.name.Length)
                                                                                                    .FirstOrDefault();
                                                currentProjectLongest.UsersAmountFilteredByProjectProperties = (project.description.Length > 20 || tasks.Count() < 3) ? _teams.Where(team => team.id == project.teamId)
                                                                                                                        .GroupJoin(_users,
                                                                                                                                    team => team.id,
                                                                                                                                    user => user.teamId,
                                                                                                                                    (team, users) => { return users.Count(); }).FirstOrDefault() : 0;
                                                return currentProjectLongest;
                                            });
            return res;

        }
           
    }
}
