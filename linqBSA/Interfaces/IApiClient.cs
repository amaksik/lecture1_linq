﻿using linqBSA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Interfaces
{

    public interface IApiClient
    {

        public  Task<List<T>> ReceiveApiData<T>(string endpoint) where T : class;

    }

}
