﻿using linqBSA.Models;
using linqBSA.Models.HirerarchyModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Interfaces
{
    interface ILogicService
    {

        Dictionary<string, int> GetTasksByUser(int id);//1
        IEnumerable<Models.Task> GetTasksByLenght(int id);//2
        IEnumerable<Models.Task> GetFinishedTasks(int id);//3
        IEnumerable<(int, string, List<User>)> GetTeamsOrderedAndSorted();//4
        IEnumerable<User> GetUsersSortedByAlphabet();//5
        UsersLastProjectWithTasksandLonger GetLastProjectCountAndLongerTasks(int userId);//6
        IEnumerable<LongestAndShortestTaskWithUsers> GetProjectLogestAndShortestTaskAndUsersAmount();//7
        
    }
}
