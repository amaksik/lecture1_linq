﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Models
{
     
    public class Project
    {

        public int id { get; set; }
        public int authorId { get; set; }
        public int teamId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime deadline { get; set; }
        public DateTime createdAt { get; set; }

        public User Author { get; set; }
        public Team Team { get; set; }
        public List<Task> Tasks { get; set; }

    }
}
