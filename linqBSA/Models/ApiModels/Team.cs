﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Models
{
    public class Team
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createdAt { get; set; }

        public List<User> Performers { get; set; }
    }
}
