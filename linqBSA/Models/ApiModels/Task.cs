﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Models
{
    public class Task
    {
        public int id { get; set; }
        public int projectId { get; set; }
        public int performerId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime? finishedAt { get; set; }


       
        public Project Project { get; set; }
        public User Performer { get; set; }      
        public StateOfTask State { get; set; }

    }
}
