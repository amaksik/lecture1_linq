﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Models
{
    public class User
    {
        public int id { get; set; }
        public int? teamId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public DateTime registeredAt { get; set; }
        public DateTime birthDay { get; set; }


      
        public List<Project> Projects { get; set; }
        public Team Team { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
