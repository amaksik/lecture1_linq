﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Models
{
    public enum StateOfTask
    {
        Created = 0,
        Started,
        InProgress,
        Finished
    }
}
