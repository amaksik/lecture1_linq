﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Models.HirerarchyModels
{
    public class UsersLastProjectWithTasksandLonger
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int CountTasksPerLastProject { get; set; }
        public int NotFinishedOrCanceledTasks { get; set; }
        public Task LongestTask { get; set; }
    }
}
