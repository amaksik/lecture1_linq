﻿using linqBSA.Models;
using linqBSA.Services;
using System;


namespace linqBSA
{
    public class ConsoleMenu
    {
        public void Run()
        {
            while (true)
            {
                Console.WriteLine(Constants.menustring);

                var option = Console.ReadLine();

                var service = new LogicService();
              
                try
                {
                    
                    switch (option)
                    {
                        case "1":
                            Console.WriteLine("Write id:");

                            var id = int.Parse(Console.ReadLine());

                            var result1 = service.GetTasksByUser(id);

                            Console.WriteLine("Result\t");

                            foreach (var item in result1)
                            {
                                Console.WriteLine(item.Key + ": " + item.Value);
                            }
                            break;


                        case "2":
                            Console.WriteLine("Write id:");

                            var Id = int.Parse(Console.ReadLine());

                            var result2 = service.GetTasksByLenght(Id);

                            foreach (var item in result2)
                            {
                                Console.WriteLine(item.name);
                            }
                            break;


                        case "3":
                            Console.WriteLine("Write id:");
                            var ID = int.Parse(Console.ReadLine());

                            var result3 = service.GetFinishedTasks(ID);
                            foreach (var item in result3)
                            {
                                Console.WriteLine(item.name);
                            }
                            break;


                        case "4":
                            var result4 = service.GetTeamsOrderedAndSorted();
                            foreach (var item in result4)
                            {
                                Console.WriteLine(item.Item2);
                                foreach (var subItem in item.Item3)
                                {
                                    Console.WriteLine(subItem.firstName + "\n");
                                }
                            }
                            break;



                        case "5":
                            var result5 = service.GetUsersSortedByAlphabet();
                            foreach (var item in result5)
                            {
                                Console.WriteLine(item.firstName);
                            }
                            break;
                        case "6":


                            Console.WriteLine("Write id:");

                            var userID = int.Parse(Console.ReadLine());

                            var result6 = service.GetLastProjectCountAndLongerTasks(userID);
                            Console.WriteLine(result6.LastProject.name + "," + result6.CountTasksPerLastProject);
                            break;


                        case "7":
                            var result7 = service.GetProjectLogestAndShortestTaskAndUsersAmount();
                            foreach (var item in result7)
                            {
                                Console.WriteLine(item.UsersAmountFilteredByProjectProperties + "," + item.Project.name + "," + item.LongestTaskByDescription.name);
                            }
                            break;



                        default:
                            Console.Write("\nCommand not found");
                            foreach (var item in ". . . ")
                            {
                                Console.Write(item);
                                System.Threading.Thread.Sleep(800);
                            }

                            Console.Clear();
                            break;
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    foreach (var item in ". . . ")
                    {
                        Console.Write(item);
                        System.Threading.Thread.Sleep(800);
                    }

                    Console.Clear();
                
                }
                Console.Write("\n\n");
                
                System.Threading.Thread.Sleep(2000);

                Console.Clear();

            }
            
        }
    }
}
