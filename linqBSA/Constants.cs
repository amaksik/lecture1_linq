﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqBSA.Models
{
    public static class Constants
    {
        public static string menustring = "1 <- Получить кол-во тасков у проекта конкретного пользователя (по Id)\n" +
                                    "2 <- Получить список тасков, назначенных на конкретного пользователя(по Id),name таска< 45 символов\n" +
                                    "3 <- Получить список(Id, name) из коллекции тасков\n" +
                                    "4 <- Получить список(Id, имя команды и список пользователей) из коллекции команд\n" +
                                    "5 <- Получить список пользователей по алфавиту first_name\n" +
                                    "6 <- Получить структуру 6\n" +
                                    "7 <- Получить  структуру 7\n";

        public static string apilink = @"https://bsa21.azurewebsites.net/api/";

        public static int maxNameLenght = 45;
        public static int olderTen = 2011;

    }
}
